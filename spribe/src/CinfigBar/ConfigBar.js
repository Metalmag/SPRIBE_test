import React, {Component} from 'react';
import {connect} from 'react-redux';
import './ConfigBar_Style.css';

import {DrawDiagram} from '../Diagram/Diagram';

class ConfigBar extends Component {
    constructor (props) {
        super (props);
        this.setBlue = this.setBlue.bind (this);
        this.setGreen = this.setGreen.bind (this);
        this.setResult = this.setResult.bind (this)

    }

    setBlue () {
        let Canvas = document.getElementById ('Canvas');
        if (this.Blue.value && 0 <= + this.Blue.value && 100 >= + this.Blue.value) {
            let delta = this.Blue.value - this.props.DiagramRedux.BluePercent;
            console.log (delta);
            if (delta !== 0) {
                if (delta > 0) {
                    let shift = setInterval (() => {
                        this.props.Set_Blue_Value (++ this.props.DiagramRedux.BluePercent);
                        console.log (this.props.DiagramRedux.BluePercent);
                        DrawDiagram (Canvas, this.props.DiagramRedux.BluePercent, this.props.DiagramRedux.GreenPercent, this.props.DiagramRedux.ResultPercent);
                        if (+ this.Blue.value === this.props.DiagramRedux.BluePercent) {
                            clearInterval (shift);
                        }
                    }, 100);
                } else {
                    let shift = setInterval (() => {
                        this.props.Set_Blue_Value (-- this.props.DiagramRedux.BluePercent);
                        console.log (this.props.DiagramRedux.BluePercent);
                        DrawDiagram (Canvas, this.props.DiagramRedux.BluePercent, this.props.DiagramRedux.GreenPercent, this.props.DiagramRedux.ResultPercent);
                        if (+ this.Blue.value === this.props.DiagramRedux.BluePercent) {
                            clearInterval (shift);
                        }
                    }, 100)
                }
            }
        } else {
            alert ('Incorrect value')
        }
    }

    setGreen () {
        let Canvas = document.getElementById ('Canvas');
        if (this.Green.value && 0 <= + this.Green.value && 100 >= + this.Green.value) {
            let delta = this.Green.value - this.props.DiagramRedux.GreenPercent;
            if (delta !== 0) {
                if (delta > 0) {
                    let shift = setInterval (() => {
                        this.props.Set_Green_Value (++ this.props.DiagramRedux.GreenPercent);
                        DrawDiagram (Canvas, this.props.DiagramRedux.BluePercent, this.props.DiagramRedux.GreenPercent, this.props.DiagramRedux.ResultPercent);
                        if (+ this.Green.value === this.props.DiagramRedux.GreenPercent) {
                            clearInterval (shift);
                        }
                    }, 100);
                } else {
                    let shift = setInterval (() => {
                        this.props.Set_Green_Value (-- this.props.DiagramRedux.GreenPercent);
                        DrawDiagram (Canvas, this.props.DiagramRedux.BluePercent, this.props.DiagramRedux.GreenPercent, this.props.DiagramRedux.ResultPercent);
                        if (+ this.Green.value === this.props.DiagramRedux.GreenPercent) {
                            clearInterval (shift);
                        }
                    }, 100)
                }
            }

        } else {
            alert ('Incorrect value')
        }
    }

    setResult () {
        if (this.Green.value && this.Blue.value){
            let Canvas = document.getElementById ('Canvas');
            this.Result.value = (+ this.Green.value + + this.Blue.value) / 2;
            let delta = this.Result.value - this.props.DiagramRedux.ResultPercent;
            if (delta !== 0) {
                if (delta > 0) {
                    let shift = setInterval (() => {
                        this.props.Set_Result_Value (0.5 + this.props.DiagramRedux.ResultPercent);
                        DrawDiagram (Canvas, this.props.DiagramRedux.BluePercent, this.props.DiagramRedux.GreenPercent, this.props.DiagramRedux.ResultPercent);
                        if (+ this.Result.value === this.props.DiagramRedux.ResultPercent) {
                            clearInterval (shift);
                        }
                    }, 50);
                } else {
                    let shift = setInterval (() => {
                        this.props.Set_Result_Value (this.props.DiagramRedux.ResultPercent -0.5);
                        DrawDiagram (Canvas, this.props.DiagramRedux.BluePercent, this.props.DiagramRedux.GreenPercent, this.props.DiagramRedux.ResultPercent);
                        if (+ this.Result.value === this.props.DiagramRedux.ResultPercent) {
                            clearInterval (shift);
                        }
                    }, 50)
                }
            }
        }
    }

    render () {
        return (
            <div className="ConfigBar">
                <div className='ConfigBar_BTN_Container'>
                    <input ref={(input) => this.Blue = input} placeholder="30" className="ConfigBar_Input"/>
                    <button onClick={this.setBlue} className="ConfigBar_BTN">Fill Blue %</button>
                </div>
                <div className='ConfigBar_BTN_Container'>
                    <input ref={(input) => this.Green = input} placeholder="70" className="ConfigBar_Input"/>
                    <button onClick={this.setGreen} className="ConfigBar_BTN">Fill Green %</button>
                </div>
                <div className='ConfigBar_BTN_Container'>
                    <input ref={(input) => this.Result = input} className="ConfigBar_Input" disabled/>
                    <button onClick={this.setResult} className="ConfigBar_BTN">Result</button>
                </div>
            </div>
        );
    }
}

export default connect (
    state => ({
        DiagramRedux: state.DiagramRedux,

    }),
    dispatch => ({
        Set_Green_Value: (percent) => {
            dispatch ({type: 'Set_Green_Percent', percent: percent})
        },
        Set_Blue_Value: (percent) => {
            dispatch ({type: 'Set_Blue_Percent', percent: percent})
        },
        Set_Result_Value: (percent) => {
            dispatch ({type: 'Set_Result_Percent', percent: percent})
        }
    })) (ConfigBar)

