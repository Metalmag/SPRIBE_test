const initialState = {
    GreenPercent:70,
    BluePercent:50,
    ResultPercent:50.00

};

export default function DiagramRedux (state = initialState, action) {
    if (action.type === 'Set_Green_Percent'){
        return {
            ...state,
            GreenPercent: action.percent
        }
    }
    if (action.type === 'Set_Blue_Percent'){
        return {
            ...state,
            BluePercent: action.percent
        }
    }
    if (action.type === 'Set_Result_Percent'){
        return {
            ...state,
            ResultPercent: action.percent
        }
    }
    return state
}