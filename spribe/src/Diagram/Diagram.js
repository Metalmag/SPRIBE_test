import React, {Component} from 'react';
import {connect} from 'react-redux';

import './Diagram_Style.css';

let canvas,
    x,
    y,
    ctx,
    LineWidth = 30,
    BlueRadius = 450,
    GreenRadius = 400,
    ResultRadius = 425,
    CountDownRadius = 365;

export function DrawDiagram (Canvas,BluePercent,GreenPercent,ResultPercent){
    x = 500;
    y = Canvas.height;
    ctx = Canvas.getContext ('2d');
    ctx.lineWidth = LineWidth;

    ctx.clearRect(0,0,1000,500);

    ctx.strokeStyle = "#442075";
    ctx.beginPath ();
    ctx.arc (x, y, BlueRadius, Math.PI, Math.PI * (2 - (BluePercent)/100), false);
    ctx.stroke ();

    ctx.strokeStyle = "#12B3F1";
    ctx.beginPath ();
    if (BluePercent !== 0){
        ctx.arc (x, y, BlueRadius, Math.PI * (2 - (BluePercent)/100), 0, false);
    }
    ctx.stroke ();

    ctx.strokeStyle = "#000"; //red
    ctx.lineWidth = 1;
    let delta = Math.PI / 100;
    for (let i = 1; i < 100; i ++) {
        ctx.beginPath ();
        let x1 = x + (BlueRadius - LineWidth / 2) * Math.cos (Math.PI + delta * i);
        let y1 = x + (BlueRadius - LineWidth / 2) * Math.sin (Math.PI + delta * i);
        ctx.moveTo (x1, y1);
        let x0 = x + (BlueRadius + LineWidth / 2) * Math.cos (Math.PI + delta * i);
        let y0 = x + (BlueRadius + LineWidth / 2) * Math.sin (Math.PI + delta * i);
        ctx.lineTo (x0, y0);
        ctx.stroke ();
    }

    ctx.lineWidth = LineWidth;
    ctx.strokeStyle = "#50E2C1";
    ctx.beginPath ();
    ctx.arc (x, y, GreenRadius, Math.PI, Math.PI * (1 +GreenPercent/100) , false);
    ctx.stroke ();
    ctx.strokeStyle = "#442075";
    ctx.beginPath ();
    if (GreenPercent !== 100){ctx.arc (x, y, GreenRadius, Math.PI * (1 + GreenPercent/100), 0, false);}

    ctx.stroke ();

    ctx.strokeStyle = "#000"; //red
    ctx.lineWidth = 1;
    for (let i = 1; i < 100; i ++) {
        ctx.beginPath ();
        let x1 = x + (GreenRadius - LineWidth / 2) * Math.cos (Math.PI + delta * i);
        let y1 = x + (GreenRadius - LineWidth / 2) * Math.sin (Math.PI + delta * i);
        ctx.moveTo (x1, y1);
        let x0 = x + (GreenRadius + LineWidth / 2) * Math.cos (Math.PI + delta * i);
        let y0 = x + (GreenRadius + LineWidth / 2) * Math.sin (Math.PI + delta * i);
        ctx.lineTo (x0, y0);
        ctx.stroke ();
    }


    ctx.lineWidth = 5;
    ctx.strokeStyle = "#442075";
    ctx.beginPath ();
    ctx.arc (x, y, ResultRadius, Math.PI,0, false);
    ctx.stroke ();

    ctx.fillStyle = "#FDC304";
    ctx.beginPath ();
    let x1 = x + (ResultRadius) * Math.cos (Math.PI*(1+ResultPercent/100));
    let y1 = x + (ResultRadius) * Math.sin (Math.PI*(1+ResultPercent/100));
    ctx.arc (x1, y1, 20, Math.PI*2, 0, false);
    ctx.fill ();

    ctx.fillStyle = "#000";
    ctx.beginPath ();
    ctx.arc (x1, y1, 5, Math.PI*2, 0, false);
    ctx.fill ();


    ctx.lineWidth = 5;
    ctx.fillStyle = "#442075";
    ctx.beginPath ();
    ctx.arc (x, y, CountDownRadius, Math.PI,0, false);
    ctx.fill ();
}

class Diagram extends Component {

    componentDidMount () {
        DrawDiagram(this.Diagram,this.props.DiagramRedux.BluePercent,this.props.DiagramRedux.GreenPercent,this.props.DiagramRedux.ResultPercent);
    }


    render () {
        return (
            <div className="Diagram">
                <canvas height='500px' width='1000px' id="Canvas" ref={(canvas) => this.Diagram = canvas}/>
                <div className="Diagram_CountDown">
                    {this.props.DiagramRedux.ResultPercent.toFixed(2)}
                </div>
            </div>
        );
    }
}
export default connect (
    state => ({
        DiagramRedux : state.DiagramRedux,

    }),
    dispatch => ({
    })) (Diagram)

