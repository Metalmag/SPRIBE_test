import React, {Component} from 'react';

import ConfigBar from './CinfigBar/ConfigBar';
import Diagram from './Diagram/Diagram';
import './App.css';

class App extends Component {
    render () {
        return (
            <div className="App">
                <ConfigBar/>
                <Diagram/>
            </div>
        );
    }
}

export default App;
